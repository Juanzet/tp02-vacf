using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DCartel : MonoBehaviour
{
    
    [SerializeField] GameObject dialogueMark;
    [SerializeField] GameObject dialoguePanel;
    [SerializeField] TMP_Text dialogueTxt;
    [SerializeField] float typingTime = 0.05f;
    [SerializeField, TextArea(4,6)] string[] dialogueLines;

    bool isPlayerInRange;
    bool didDialogueStart;
    int lineIndex;

     void Update() 
    {
        if(isPlayerInRange && Input.GetKeyDown(KeyCode.E))
        {
            if(!didDialogueStart)
            {
                StartDialogue();
            }
            else if(dialogueTxt.text == dialogueLines[lineIndex])
            {
                NextDialogueLine();
            }
            else 
            {
                StopAllCoroutines();
                dialogueTxt.text = dialogueLines[lineIndex];
            }
        }    
    }

    void StartDialogue()
    {
        didDialogueStart = true;
        dialoguePanel.SetActive(true);
        dialogueMark.SetActive(false);
        lineIndex = 0;
        Time.timeScale = 0f;
        StartCoroutine(ShowLine());
    }

    void NextDialogueLine()
    {
        lineIndex++;
        if(lineIndex<dialogueLines.Length)
        {
            StartCoroutine(ShowLine());
        }
        else 
        {
            didDialogueStart = false; 
            dialoguePanel.SetActive(false);
            dialogueMark.SetActive(false);
            Time.timeScale = 1;
        }
    }
    
    IEnumerator ShowLine()
    {
        dialogueTxt.text = string.Empty;

        foreach (char c in dialogueLines[lineIndex])
        {
            dialogueTxt.text += c;
            yield return new WaitForSecondsRealtime(typingTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = true;
            Debug.Log("Entro");
            dialogueMark.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {  
        if(other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = false;
            Debug.Log("Salio");
            dialogueMark.SetActive(false);
        }
    }
}
