using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }


    [SerializeField] int indexDialogue;
    [SerializeField] float secToChangeScene;

    void Awake() 
    {    
        // If there is an instance, and it's not me, delete myself.
    
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
    }

    public void EventDialogue(int number)
    {
        if(number == indexDialogue)
        {
            StartCoroutine(Wait());
            
        } 
        else 
        {
            Debug.Log("No hay evento");
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSecondsRealtime(secToChangeScene);
        SceneManager.LoadScene("Level02");
    }

     
}
