using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] Rigidbody2D rb;
    Vector2 moveDirection;
    float horizontal;
    float vertical;


     
    void Start()
    {
        
    }

     
    void Update()
    {
        Inputs();
    }

    void Inputs()
    {
        //movimiento
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");        

        moveDirection = new Vector2(horizontal,vertical).normalized;
        rb.velocity = new Vector2(moveDirection.x * moveSpeed, moveDirection.y * moveSpeed);
    }

}
