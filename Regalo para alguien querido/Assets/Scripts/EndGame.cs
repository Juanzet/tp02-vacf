using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EndGame : MonoBehaviour
{
    bool isPlayerInRange;

    [SerializeField] string content;
    void Update()
    {
        if(isPlayerInRange && Input.GetKeyDown(KeyCode.E))
        {
            OnApplicationQuit();
            Application.Quit();
        } 
    }


    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = true;
            
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {  
        if(other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = false;
 
        }
    }

    private void OnApplicationQuit()
    {
        string desktopPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
        string filePath = Path.Combine(desktopPath, "LEER_AL_FINALIZAR.txt");
        //string content = "Gracias por jugar mi juego!";

        File.WriteAllText(filePath, content);
    }
}
