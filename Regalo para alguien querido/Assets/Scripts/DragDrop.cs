using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragDrop : MonoBehaviour
{
    [SerializeField] GameObject objToDragToPos;
    [SerializeField] GameObject objToDrag;
    [SerializeField] float dropDistance;
    bool isLocked;
    Vector2 objInitPos;

    void Start()
    {
        objInitPos = objToDrag.transform.position;
    }
    
    public void DragObject()
    {
        if(!isLocked)
        {
            objToDrag.transform.position = Input.mousePosition;
        }
    }

    public void DropObject()
    {
        float distance = Vector3.Distance(objToDrag.transform.position, objToDragToPos.transform.position);
        if(distance < dropDistance)
        {
            PointsManager.Instance.AddPoints();
            isLocked = true;
            objToDrag.transform.position = objToDragToPos.transform.position;
        }
        else 
        {
            objToDrag.transform.position = objInitPos;
        }
    }
}
