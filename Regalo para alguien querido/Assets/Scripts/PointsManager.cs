using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PointsManager : MonoBehaviour
{
    public static PointsManager Instance { get; private set; }

    int points;

    void Awake() 
    { 
        // If there is an instance, and it's not me, delete myself.
    
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
    }

    public void AddPoints()
    {
        Debug.Log(points);
        points++;
        WinGame();
    }

    void WinGame()
    {
        if(points >= 3)
        {
            SceneManager.LoadScene("Win");
        }
        else 
        {
            Debug.Log($"Faltan puntos para ganar");
        }
    }

}
